// Task 2.1
function bubbleClick() {
    let list = document.getElementsByClassName("bubble");
    let counter = 0;
    for (let index = 0; index < list.length; index++) {
        list[index].click();
        counter++;
    }
    console.log(counter == document.getElementById('score').innerText);
}
setTimeout(bubbleClick, 5000);


// Task 2.2
const str = "mic09ha1el 4b5en6 michelle be4atr3ice";
function superSort(value) {
    let arr = value.split(" ");
    let modArr = [];
    arr.forEach(element => {
        modArr.push({"element" : element, "modElement" : element.replace(/[0-9]/g, "")})
    });

    modArr.sort((a, b) => (a.modElement > b.modElement) ? 1 : -1)
    let sortedArr = [];
    modArr.forEach(item => {
        sortedArr.push(item.element)
    });

    return sortedArr.join(" ");
}
// console.log(superSort(str));


// Task 2.3
var date1 = "23-03.3452";
var date2 = "12,04/3452"; 
function compareDates(date1, date2) {
    let firstArr = date1.split(/[,-.\/]/);
    let secondArr = date2.split(/[,-.\/]/);

    if (firstArr[0].length == 4 && secondArr[0].length == 4) {
        return firstArr[0] == secondArr[0] && firstArr[1] == secondArr[1] && firstArr[2] == secondArr[2];
    } else if (firstArr[2].length == 4 && secondArr[2].length == 4) {
        return firstArr[0] == secondArr[0] && firstArr[1] == secondArr[1] && firstArr[2] == secondArr[2];
    } else {
        return firstArr[0] == secondArr[2] && firstArr[1] == secondArr[0] && firstArr[2] == secondArr[1];
    }
}
// compareDates(date1, date2);


// Task 2.4
var testArr = [0, 0, 0, 55, 0, 0];
function findUnique(someArr) {
    var mySet = new Set(someArr);
    var uniqueArr = [];
    mySet.forEach(element => {
        uniqueArr.push(element);
    });
    var count1 = 0;
    var count2 = 0;

    for (let index = 0; index < someArr.length; index++) {
        if (uniqueArr[0] == someArr[index]) {
            count1++;
            if (count1 > 1) {
                return uniqueArr[1];
            }
        } else {
            count2++;
            if (count2 > 1) {
                return uniqueArr[0];
            }
        }
    }
}
// console.log(findUnique(testArr));


// Task 2.5
var price = 100;
function getDiscount(number) {
    if ((number >= 5) && (number < 10)) {
        return price * 0.95;
    } else if (number >= 10) {
        return price*0.9;
    } else {
        return price;
    }
}
// console.log(getDiscount(34));


// Task 2.6
function getDaysForMonth(month) {
    let message = "You have entered wrong value. Month have to be > 0 and <= 12";
    switch (month) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            return 31;
        case 2:
            return 28;
        case 4:
        case 6:
        case 9:
        case 11:
            return 30;
        default:
            return message;
    }
}
// console.log(getDaysForMonth(7));